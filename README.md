# PushBox


## How To Compile : Compile in Visual Studio ##
Add "winmm.lib" in  Project Properties->Configuration Properties->Linker->Input->Additional Dependencies  
Sounds(wavs) must be in the same folder of the .cpp if you use the compiler, and if you use directly the exe, must be in .exe folder. 


## How to view Properly ##
Once compiled/running go to Console Properties->Fonts->Raster Fonts->Set 8x8  
Use always the keyword "E" always to accept

## Useful Resources ##

Juego creado por dos alumnos de la UPC para el curso de Programacion. Creado en C++.

-  **Project Source/Data** : https://1drv.ms/f/s!AlBOXn97PZ--vSXBcMFImNp-z1WV
-  **Executables Only** : https://1drv.ms/f/s!AlBOXn97PZ--vUfBcMFImNp-z1WV
- **Speed GamePlay** : https://youtu.be/BuCM0NR5vNM
- **Full Video**  : https://youtu.be/qKXpoMNdYYc
- **Source Code / Código** : https://gitlab.com/saintplay/PushBox (code is full spanish)
- **Written in / Escrito en** : Full C++, Visual Studio C++ Console Project
- **Time/ Tiempo** : 3 months/ 3 meses

### Trabajo Final de Programación I ###


#### Juego creado por: ####

- Diego Jara Palomino

- Jeremy Tornero Landeo


#### Agradecimientos especiales: ####

- Carlos Mere
- Brayan Cruces(por desanimarnos)

### UPC - Ciclo 2014 - 02 ###

Profesor : Juan Ramirez Espinoza
